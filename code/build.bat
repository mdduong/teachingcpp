@echo off

if not exist ..\build mkdir ..\build
pushd ..\build

clang++ -g -Wall ..\code\hellomom.cpp -o hellomom.exe

popd